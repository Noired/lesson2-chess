package it.polimi.model.pieces;

import it.polimi.model.Color;
import it.polimi.model.Position;

/**
 * Created by marcofunaro on 4/21/15.
 */
public interface Piece {
    Boolean canMove(Position from, Position to);
    Color getColor();
}
