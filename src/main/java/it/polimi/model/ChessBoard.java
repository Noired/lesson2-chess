package it.polimi.model;

import it.polimi.common.observer.BaseObservable;
import it.polimi.common.observer.Event;
import it.polimi.model.pieces.King;
import it.polimi.model.pieces.Piece;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcofunaro on 4/21/15.
 */
public class ChessBoard extends BaseObservable{
    public static final Position STARTING_POSITION = new Position('D', 4);
    private final Map<Position, Piece> pieces;

    public ChessBoard() {
        this.pieces = new HashMap<Position, Piece>();
        pieces.put(STARTING_POSITION, new King(Color.BLACK));
    }

    public void move(Position from, Position to){
        if(pieces.containsKey(from)
                && pieces.get(from).canMove(from, to) ){
            Piece king = pieces.get(from);
            pieces.remove(from);
            pieces.put(to, king);
            notify(new Event(String.format("Now the king is in position %s", to.toString())));
        } else {
            super.notify(new Event("no piece in the from position!!"));
        }
    }
}
