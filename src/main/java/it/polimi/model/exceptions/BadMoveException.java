package it.polimi.model.exceptions;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class BadMoveException extends ChessException {
    public BadMoveException(String message) {
        super(message);
    }
}
