package it.polimi.common.observer;

/**
 * Created by marcofunaro on 4/18/15.
 */
public final class Event {
    private final String msg;

    public Event(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
