package it.polimi.view;

import it.polimi.common.observer.Observable;
import it.polimi.common.observer.Observer;
import it.polimi.common.observer.Event;


/**
 * Created by marcofunaro on 4/18/15.
 */
public class EventWriter implements Observer {

    public void notify(Observable source, Event event) {
        System.out.println(event.getMsg());
    }
}
